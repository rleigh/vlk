//
// Created by rleigh on 29/06/2020.
//

#include "Window.h"
#include <stdexcept>

namespace {}

Window::Window(const char *title, int width, int height) {
  glfwInit();

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

  this->window = glfwCreateWindow(width, height, title, nullptr, nullptr);

  glfwSetWindowUserPointer(window, this);
  glfwSetFramebufferSizeCallback(window, windowResizeCallback);
  glfwSetWindowFocusCallback(window, windowFocusCallback);
  glfwSetWindowCloseCallback(window, windowCloseCallback);
  glfwSetKeyCallback(window, keyPressCallback);
  glfwSetMouseButtonCallback(window, mouseButtonPressCallback);
  glfwSetScrollCallback(window, scrollCallback);
  glfwSetCursorPosCallback(window, cursorPositionCallback);
  glfwSetCursorEnterCallback(window, cursorEnterCallback);
}

Window::~Window() {
  glfwDestroyWindow(window);

  glfwTerminate();
}

void Window::windowResizeCallback(GLFWwindow *window, int width, int height) {
  Window *w = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  w->windowResize(width, height);
}

void Window::windowCloseCallback(GLFWwindow *window) {
  Window *w = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  w->windowClose();
}

void Window::windowFocusCallback(GLFWwindow *window, int focus) {
  Window *w = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  w->windowFocus(focus != 0);
}

void Window::keyPressCallback(GLFWwindow *window, int key, int scancode,
                              int action, int mods) {
  Window *w = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  w->keyPress(key, scancode, action, mods);
}

void Window::mouseButtonPressCallback(GLFWwindow *window, int button,
                                      int action, int mods) {
  Window *w = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  w->mouseButtonPress(button, action, mods);
}

void Window::scrollCallback(GLFWwindow *window, double xoffset,
                            double yoffset) {
  Window *w = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  w->scroll(xoffset, yoffset);
}

void Window::cursorPositionCallback(GLFWwindow *window, double xpos,
                                    double ypos) {
  Window *w = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  w->cursorPosition(xpos, ypos);
}

void Window::cursorEnterCallback(GLFWwindow *window, int enter) {
  Window *w = reinterpret_cast<Window *>(glfwGetWindowUserPointer(window));
  w->cursorEnter(enter);
}

void Window::windowResize(int width, int height) {}

void Window::windowClose() {}

void Window::windowFocus(bool focus) {}

void Window::keyPress(int key, int scancode, int action, int mods) {}

void Window::mouseButtonPress(int button, int action, int mods) {}

void Window::scroll(double xoffset, double yoffset) {}

void Window::cursorPosition(double xpos, double ypos) {}

void Window::cursorEnter(bool enter) {}

void Window::run() { mainLoop(); }

std::pair<int, int> Window::size() const {
  int width, height;

  glfwGetFramebufferSize(window, &width, &height);

  return std::pair(width, height);
}

void Window::mainLoop() {
  while (!glfwWindowShouldClose(window)) {
    glfwPollEvents();

    auto windowSize = size();
    if (windowSize.first == 0 || windowSize.second == 0) {
      waitEvents();
    } else {
      draw();
    }
  }
}

vk::Result Window::createSurface(vk::Instance instance,
                                 vk::SurfaceKHR &surface) {
  VkSurfaceKHR vksurface;

  auto result = static_cast<vk::Result>(
      glfwCreateWindowSurface(instance, window, nullptr, &vksurface));

  if (result != vk::Result::eSuccess) {
    throw std::runtime_error("failed to create window surface!");
  }

  surface = vksurface;

  return result;
}

void Window::draw() {}

void Window::waitEvents() { glfwWaitEvents(); }
