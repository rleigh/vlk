//
// Created by rleigh on 13/07/2020.
//

#ifndef VLK_APPLICATIONWINDOW_H
#define VLK_APPLICATIONWINDOW_H

#include "Window.h"

#include <optional>
#include <span>
#include <string>
#include <vector>

#define VULKAN_HPP_DISPATCH_LOADER_DYNAMIC 1
#include <vulkan/vulkan.hpp>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>
#include <glm/gtx/string_cast.hpp>

#include <sol/sol.hpp>

struct SwapChainSupportDetails {
  vk::SurfaceCapabilitiesKHR capabilities;
  std::vector<vk::SurfaceFormatKHR> formats;
  std::vector<vk::PresentModeKHR> presentModes;
};

struct QueueFamilyIndices {
  std::optional<uint32_t> graphicsFamily;
  std::optional<uint32_t> presentFamily;

  bool isComplete() const {
    return graphicsFamily.has_value() && presentFamily.has_value();
  }
};

struct Vertex {
  glm::vec3 pos;
  glm::vec3 color;
  glm::vec2 texCoord;

  bool operator==(const Vertex &rhs) const {
    return (pos == rhs.pos) && (color == rhs.color) &&
           (texCoord == rhs.texCoord);
  }

  static vk::VertexInputBindingDescription getBindingDescription() {
    auto bindingDescription = vk::VertexInputBindingDescription{
        0u, static_cast<uint32_t>(sizeof(Vertex)),
        vk::VertexInputRate::eVertex};
    return bindingDescription;
  }

  static std::array<vk::VertexInputAttributeDescription, 3>
  getAttributeDescriptions() {
    auto attributeDescriptions =
        std::array<vk::VertexInputAttributeDescription, 3>{
            {{0u, 0u, vk::Format::eR32G32B32Sfloat,
              static_cast<uint32_t>(offsetof(Vertex, pos))},
             {1u, 0u, vk::Format::eR32G32B32Sfloat,
              static_cast<uint32_t>(offsetof(Vertex, color))},
             {2u, 0u, vk::Format::eR32G32Sfloat,
              static_cast<uint32_t>(offsetof(Vertex, texCoord))}}};

    return attributeDescriptions;
  }
};

template <> struct std::hash<Vertex> {
  size_t operator()(Vertex const &vertex) const noexcept {
    std::size_t h1 = std::hash<glm::vec3>{}(vertex.pos);
    std::size_t h2 = std::hash<glm::vec3>{}(vertex.color);
    std::size_t h3 = std::hash<glm::vec2>{}(vertex.texCoord);
    return ((h1 ^ (h2 << 1)) >> 1) ^ (h3 << 1);
  }
};

class ApplicationWindow : public Window {
public:
  explicit ApplicationWindow(const char *appName);

  ~ApplicationWindow() override;

  void windowResize(int width, int height) override;
  void windowFocus(bool focus) override;
  void windowClose() override;
  void keyPress(int key, int scancode, int action, int mods) override;
  void mouseButtonPress(int button, int action, int mods) override;
  void scroll(double xoffset, double yoffset) override;
  void cursorPosition(double xpos, double ypos) override;
  void cursorEnter(bool enter) override;

  void initVulkan();
  void draw() override;
  void cleanupSwapChain();
  void cleanup();
  void recreateSwapChain();
  void createInstance();
  void setupDebugMessenger();
  void pickPhysicalDevice();
  void createLogicalDevice();
  void createSwapChain();
  void createImageViews();
  void createRenderPass();
  void createDescriptorSetLayout();
  void createGraphicsPipeline();
  void createFramebuffers();
  void createCommandPool();
  void createDepthResources();
  void createColorResources();
  void createTextureImage();
  void createTextureImageView();
  void createTextureSampler();
  void createImage(uint32_t width, uint32_t height,
                   vk::SampleCountFlagBits numSamples, uint32_t mipLevels,
                   vk::Format format, vk::ImageTiling tiling,
                   vk::ImageUsageFlags usage,
                   vk::MemoryPropertyFlags properties, vk::Image &image,
                   vk::DeviceMemory &imageMemory);
  vk::ImageView createImageView(vk::Image image, vk::Format format,
                                vk::ImageAspectFlags aspectFlags,
                                uint32_t mipLevels);
  void generateMipMaps(vk::Image image, vk::Format imageFormat,
                       int32_t texWidth, int32_t texHeight, uint32_t mipLevels);
  void loadModel();
  void createVertexBuffer();
  void createIndexBuffer();
  void createUniformBuffers();
  void createDescriptorPool();
  void createDescriptorSets();
  void createBuffer(vk::DeviceSize size, vk::BufferUsageFlags usage,
                    vk::MemoryPropertyFlags properties, vk::Buffer &buffer,
                    vk::DeviceMemory &bufferMemory);
  void copyBuffer(vk::Buffer srcBuffer, vk::Buffer dstBuffer,
                  vk::DeviceSize size);
  uint32_t findMemoryType(uint32_t typeFilter,
                          vk::MemoryPropertyFlags properties);
  vk::CommandBuffer beginSingleTimeCommands();
  void endSingleTimeCommands(vk::CommandBuffer commandBuffer);

  void transitionImageLayout(vk::Image image, vk::Format format,
                             vk::ImageLayout oldLayout,
                             vk::ImageLayout newLayout, uint32_t mipLevels);

  void copyBufferToImage(vk::Buffer buffer, vk::Image image, uint32_t width,
                         uint32_t height);

  void createCommandBuffers();
  void recordCommandBuffer(vk::CommandBuffer commandBuffer,
                           uint32_t imageIndex);
  void createSyncObjects();
  void updateUniformBuffer(uint32_t currentImage);
  void drawFrame();
  vk::ShaderModule createShaderModule(std::span<const unsigned char> code);
  vk::SurfaceFormatKHR chooseSwapSurfaceFormat(
      const std::vector<vk::SurfaceFormatKHR> &availableFormats);
  vk::PresentModeKHR chooseSwapPresentMode(
      const std::vector<vk::PresentModeKHR> &availablePresentModes);
  vk::Extent2D chooseSwapExtent(const vk::SurfaceCapabilitiesKHR &capabilities);
  SwapChainSupportDetails querySwapChainSupport(vk::PhysicalDevice device);
  bool isDeviceSuitable(vk::PhysicalDevice device);
  bool checkDeviceExtensionSupport(vk::PhysicalDevice device);
  QueueFamilyIndices findQueueFamilies(vk::PhysicalDevice device);
  std::vector<const char *> getRequiredExtensions();
  bool checkValidationLayerSupport();
  vk::Format findSupportedFormat(const std::vector<vk::Format> &candidates,
                                 vk::ImageTiling tiling,
                                 vk::FormatFeatureFlags features);
  vk::Format findDepthFormat();
  bool hasStencilComponent(vk::Format format);
  vk::SampleCountFlagBits getMaxUsableSampleCount();
  static VKAPI_ATTR VkBool32 VKAPI_CALL
  debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
                VkDebugUtilsMessageTypeFlagsEXT messageType,
                const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
                void *pUserData);

private:
  std::vector<const char *> validationLayers;
  vk::Instance instance;
  vk::DebugUtilsMessengerEXT debugMessenger;
  vk::SurfaceKHR surface;

  vk::PhysicalDevice physicalDevice;
  vk::Device device;

  vk::Queue graphicsQueue;
  vk::Queue presentQueue;

  vk::SwapchainKHR swapChain;
  std::vector<vk::Image> swapChainImages;
  vk::Format swapChainImageFormat;
  vk::Extent2D swapChainExtent;
  std::vector<vk::ImageView> swapChainImageViews;
  std::vector<vk::Framebuffer> swapChainFramebuffers;

  vk::RenderPass renderPass;
  vk::DescriptorSetLayout descriptorSetLayout;
  vk::PipelineLayout pipelineLayout;
  vk::Pipeline graphicsPipeline;

  vk::CommandPool commandPool;

  std::vector<Vertex> vertices;
  std::vector<uint32_t> indices;
  vk::Buffer vertexBuffer;
  vk::DeviceMemory vertexBufferMemory;
  vk::Buffer indexBuffer;
  vk::DeviceMemory indexBufferMemory;

  std::vector<vk::Buffer> uniformBuffers;
  std::vector<vk::DeviceMemory> uniformBuffersMemory;

  uint32_t mipLevels;
  vk::Image textureImage;
  vk::DeviceMemory textureImageMemory;
  vk::ImageView textureImageView;
  vk::Sampler textureSampler;

  vk::Image depthImage;
  vk::DeviceMemory depthImageMemory;
  vk::ImageView depthImageView;

  vk::Image colorImage;
  vk::DeviceMemory colorImageMemory;
  vk::ImageView colorImageView;

  vk::DescriptorPool descriptorPool;
  std::vector<vk::DescriptorSet> descriptorSets;

  std::vector<vk::CommandBuffer> commandBuffers;

  std::vector<vk::Semaphore> imageAvailableSemaphores;
  std::vector<vk::Semaphore> renderFinishedSemaphores;
  std::vector<vk::Fence> inFlightFences;
  size_t currentFrame = 0;
  vk::SampleCountFlagBits msaaSamples = vk::SampleCountFlagBits::e1;

  bool framebufferResized = false;

  sol::state lua;
};

#endif // VLK_APPLICATIONWINDOW_H
