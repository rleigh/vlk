//
// Created by rleigh on 13/07/2020.
//

#include "ApplicationWindow.h"
#include "FileUtils.h"
#include "UniformBufferObject.h"

#include "spdlog/spdlog.h"

#include "shader_frag.h"
#include "shader_vert.h"

#include "lua_main.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#include <vulkan/vulkan.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <algorithm>
#include <array>
#include <chrono>
#include <cstdlib>
#include <cstring>
#include <direct.h>
#include <fstream>
#include <iostream>
#include <set>
#include <stdexcept>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

#include <unordered_map>

VULKAN_HPP_DEFAULT_DISPATCH_LOADER_DYNAMIC_STORAGE

const int MAX_FRAMES_IN_FLIGHT = 2;

const std::vector<const char *> deviceExtensions = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME};

#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

const std::string MODEL_PATH = "models/viking_room.obj";
const std::string TEXTURE_PATH = "textures/viking_room.png";

ApplicationWindow::ApplicationWindow(const char *appName) : Window(appName) {
  initVulkan();

  lua.open_libraries(sol::lib::base, sol::lib::package, sol::lib::math,
                     sol::lib::debug, sol::lib::string, sol::lib::table);

  lua.require_script(
      "main", sol::string_view{reinterpret_cast<const char *>(&lua_main[0]),
                               sizeof(lua_main)});

  lua.new_usertype<glm::mat3>(
      "mat3",
      sol::constructors<glm::mat3(), glm::mat3(float const &),
                        glm::mat3(float const &, float const &, float const &,
                                  float const &, float const &, float const &,
                                  float const &, float const &,
                                  float const &)>(),
      sol::meta_function::multiplication,
      sol::resolve<glm::mat3(glm::mat3 const &, glm::mat3 const &)>(
          &glm::operator*),
      sol::meta_function::addition,
      sol::resolve<glm::mat3(glm::mat3 const &, glm::mat3 const &)>(
          &glm::operator+),
      sol::meta_function::subtraction,
      sol::resolve<glm::mat3(glm::mat3 const &, glm::mat3 const &)>(
          &glm::operator-));

  lua.new_usertype<glm::mat4>(
      "mat4",
      sol::constructors<glm::mat4(), glm::mat4(float const &),
                        glm::mat4(float const &, float const &, float const &,
                                  float const &, float const &, float const &,
                                  float const &, float const &, float const &,
                                  float const &, float const &, float const &,
                                  float const &, float const &, float const &,
                                  float const &)>(),
      sol::meta_function::multiplication,
      sol::resolve<glm::mat4(glm::mat4 const &, glm::mat4 const &)>(
          &glm::operator*),
      sol::meta_function::addition,
      sol::resolve<glm::mat4(glm::mat4 const &, glm::mat4 const &)>(
          &glm::operator+),
      sol::meta_function::subtraction,
      sol::resolve<glm::mat4(glm::mat4 const &, glm::mat4 const &)>(
          &glm::operator-));

  lua.new_usertype<glm::vec3>(
      "vec3",
      sol::constructors<glm::vec3(), glm::vec3(float const &),
                        glm::vec3(float const &, float const &,
                                  float const &)>(),
      sol::meta_function::multiplication,
      sol::resolve<glm::vec3(glm::vec3 const &, glm::vec3 const &)>(
          &glm::operator*),
      sol::meta_function::addition,
      sol::resolve<glm::vec3(glm::vec3 const &, glm::vec3 const &)>(
          &glm::operator+),
      sol::meta_function::subtraction,
      sol::resolve<glm::vec3(glm::vec3 const &, glm::vec3 const &)>(
          &glm::operator-));

  lua["mat"] = glm::mat4(1.0f);
  lua["vec"] = glm::vec3{2.0, 3.1, 82.3};

  lua.script(
      "a=mat4.new(1)\n"
      "b=mat4.new(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16)\n"
      "c = a * b\n"
      "print(c)\n"
      "d = b + b\n"
      "print(d)\n");
  lua.script("print(mat)");
  lua.script("print(vec)");
  lua.script("d = mat * mat");

  lua["application"] = this;
}

ApplicationWindow::~ApplicationWindow() { cleanup(); }

void ApplicationWindow::windowResize(int width, int height) {
  framebufferResized = true;
}

void ApplicationWindow::windowFocus(bool focus) {
  spdlog::info("Window windowFocus change: {}", focus);
}

void ApplicationWindow::windowClose() { spdlog::info("Window windowClose"); }

void ApplicationWindow::keyPress(int key, int scancode, int action, int mods) {
  spdlog::info("Window keyPress: {}, {}, {}, {}", key, scancode, action, mods);
}

void ApplicationWindow::mouseButtonPress(int button, int action, int mods) {
  spdlog::info("Window mouseButtonPress: {}, {}, {}", button, action, mods);
}

void ApplicationWindow::scroll(double xoffset, double yoffset) {
  spdlog::info("Window scroll: {}, {}", xoffset, yoffset);
}

void ApplicationWindow::cursorPosition(double xpos, double ypos) {
  spdlog::info("Cursor position: {}, {}", xpos, ypos);
}

void ApplicationWindow::cursorEnter(bool enter) {
  spdlog::info("Cursor enter: {}", enter);
}

void ApplicationWindow::initVulkan() {
  createInstance();
  setupDebugMessenger();
  createSurface(instance, surface);
  pickPhysicalDevice();
  createLogicalDevice();
  createSwapChain();
  createImageViews();
  createRenderPass();
  createDescriptorSetLayout();
  createGraphicsPipeline();
  createCommandPool();
  createDepthResources();
  createColorResources();
  createFramebuffers();
  createTextureImage();
  createTextureImageView();
  createTextureSampler();
  loadModel();
  createVertexBuffer();
  createIndexBuffer();
  createUniformBuffers();
  createDescriptorPool();
  createDescriptorSets();
  createCommandBuffers();
  createSyncObjects();
}

void ApplicationWindow::ApplicationWindow::draw() { drawFrame(); }

void ApplicationWindow::cleanupSwapChain() {
  device.waitIdle();

  device.destroy(colorImageView);
  device.destroy(colorImage);
  device.free(colorImageMemory);

  device.destroy(depthImageView);
  device.destroy(depthImage);
  device.free(depthImageMemory);

  for (auto framebuffer : swapChainFramebuffers) {
    device.destroy(framebuffer);
  }

  for (auto imageView : swapChainImageViews) {
    device.destroy(imageView);
  }

  device.destroy(swapChain);
}

void ApplicationWindow::cleanup() {
  cleanupSwapChain();

  device.destroy(graphicsPipeline);
  device.destroy(pipelineLayout);
  device.destroy(renderPass);

  for (size_t i = 0; i < swapChainImages.size(); i++) {
    device.destroy(uniformBuffers[i]);
    device.free(uniformBuffersMemory[i]);
  }

  device.destroy(descriptorPool, nullptr);

  device.destroy(textureSampler);
  device.destroy(textureImageView);
  device.destroy(textureImage);
  device.free(textureImageMemory);

  device.destroy(descriptorSetLayout);

  device.destroy(indexBuffer);
  device.free(indexBufferMemory);

  device.destroy(vertexBuffer);
  device.free(vertexBufferMemory);

  for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
    device.destroy(renderFinishedSemaphores[i]);
    device.destroy(imageAvailableSemaphores[i]);
    device.destroy(inFlightFences[i]);
  }

  device.destroy(commandPool);

  vkDestroyDevice(device, nullptr);

  if (enableValidationLayers) {
    instance.destroyDebugUtilsMessengerEXT(debugMessenger, nullptr);
  }

  instance.destroy(surface);
  vkDestroyInstance(instance, nullptr);
}

void ApplicationWindow::recreateSwapChain() {
  cleanupSwapChain();

  createSwapChain();
  createImageViews();
  createColorResources();
  createDepthResources();
  createFramebuffers();
}

void ApplicationWindow::createInstance() {
  vk::DynamicLoader dl;
  PFN_vkGetInstanceProcAddr vkGetInstanceProcAddr =
      dl.getProcAddress<PFN_vkGetInstanceProcAddr>("vkGetInstanceProcAddr");
  VULKAN_HPP_DEFAULT_DISPATCHER.init(vkGetInstanceProcAddr);

  auto appInfo = vk::ApplicationInfo(
      "VulkanTutorial", vk::makeApiVersion(0, 1, 0, 0), "No engine",
      vk::makeApiVersion(0, 1, 0, 0), vk::ApiVersion10);

  auto extensions = getRequiredExtensions();

  auto createInfo = vk::InstanceCreateInfo(
      vk::InstanceCreateFlags{}, &appInfo,
      static_cast<uint32_t>(validationLayers.size()), validationLayers.data(),
      static_cast<uint32_t>(extensions.size()), extensions.data());

  auto debugCreateInfo = vk::DebugUtilsMessengerCreateInfoEXT(
      vk::DebugUtilsMessengerCreateFlagBitsEXT{},
      vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
          vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
          vk::DebugUtilsMessageSeverityFlagBitsEXT::eError,
      vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
          vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
          vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance,
      debugCallback);

  if (enableValidationLayers) {
    createInfo.pNext = &debugCreateInfo;
    validationLayers.push_back("VK_LAYER_KHRONOS_validation");
  }

  auto result = vk::createInstance(&createInfo, nullptr, &this->instance);
  if (result != vk::Result::eSuccess) {
    throw std::runtime_error("failed to create instance!");
  }

  VULKAN_HPP_DEFAULT_DISPATCHER.init(instance);

  if (enableValidationLayers && !checkValidationLayerSupport()) {
    throw std::runtime_error("validation layers requested, but not available!");
  }
}

void ApplicationWindow::setupDebugMessenger() {
  if (!enableValidationLayers)
    return;

  auto createInfo = vk::DebugUtilsMessengerCreateInfoEXT(
      vk::DebugUtilsMessengerCreateFlagsEXT{},
      (vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose |
       vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning |
       vk::DebugUtilsMessageSeverityFlagBitsEXT::eError),
      (vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral |
       vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation |
       vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance),
      debugCallback, nullptr);

  debugMessenger = instance.createDebugUtilsMessengerEXT(createInfo, nullptr);
}

void ApplicationWindow::pickPhysicalDevice() {
  uint32_t deviceCount = 0;

  std::vector<vk::PhysicalDevice> devices = instance.enumeratePhysicalDevices();

  if (devices.empty()) {
    throw std::runtime_error("failed to find GPUs with Vulkan support!");
  }

  for (const auto &device : devices) {
    if (isDeviceSuitable(device)) {
      physicalDevice = device;
      msaaSamples = getMaxUsableSampleCount();
      break;
    }
  }

  if (!physicalDevice) {
    throw std::runtime_error("failed to find a suitable GPU!");
  }
}

void ApplicationWindow::createLogicalDevice() {
  QueueFamilyIndices indices = findQueueFamilies(physicalDevice);

  auto queueCreateInfos = std::vector<vk::DeviceQueueCreateInfo>{};
  auto uniqueQueueFamilies = std::set<uint32_t>{indices.graphicsFamily.value(),
                                                indices.presentFamily.value()};
  const float queuePriority = 1.0f;

  for (uint32_t queueFamily : uniqueQueueFamilies) {
    auto queueCreateInfo = vk::DeviceQueueCreateInfo{
        vk::DeviceQueueCreateFlags{}, queueFamily, 1u, &queuePriority};
    queueCreateInfos.push_back(queueCreateInfo);
  }

  auto deviceFeatures = vk::PhysicalDeviceFeatures{};
  deviceFeatures.setSamplerAnisotropy(true);
  deviceFeatures.setSampleRateShading(true);

  auto createInfo =
      vk::DeviceCreateInfo{vk::DeviceCreateFlags{},
                           static_cast<uint32_t>(queueCreateInfos.size()),
                           queueCreateInfos.data(),
                           0u,
                           nullptr,
                           static_cast<uint32_t>(deviceExtensions.size()),
                           deviceExtensions.data(),
                           &deviceFeatures};

  auto result = physicalDevice.createDevice(&createInfo, nullptr, &device);
  if (result != vk::Result::eSuccess) {
    throw std::runtime_error("failed to create logical device!");
  }

  VULKAN_HPP_DEFAULT_DISPATCHER.init(device);

  device.getQueue(indices.graphicsFamily.value(), 0, &graphicsQueue);
  device.getQueue(indices.presentFamily.value(), 0, &presentQueue);
}

void ApplicationWindow::createSwapChain() {
  SwapChainSupportDetails swapChainSupport =
      querySwapChainSupport(physicalDevice);

  vk::SurfaceFormatKHR surfaceFormat =
      chooseSwapSurfaceFormat(swapChainSupport.formats);
  vk::PresentModeKHR presentMode =
      chooseSwapPresentMode(swapChainSupport.presentModes);
  vk::Extent2D extent = chooseSwapExtent(swapChainSupport.capabilities);

  uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
  if (swapChainSupport.capabilities.maxImageCount > 0 &&
      imageCount > swapChainSupport.capabilities.maxImageCount) {
    imageCount = swapChainSupport.capabilities.maxImageCount;
  }

  QueueFamilyIndices indices = findQueueFamilies(physicalDevice);
  uint32_t queueFamilyIndices[] = {indices.graphicsFamily.value(),
                                   indices.presentFamily.value()};

  auto createInfo = vk::SwapchainCreateInfoKHR{
      vk::SwapchainCreateFlagsKHR{},
      surface,
      imageCount,
      surfaceFormat.format,
      surfaceFormat.colorSpace,
      extent,
      1u,
      vk::ImageUsageFlagBits::eColorAttachment,
      ((indices.graphicsFamily == indices.presentFamily)
           ? vk::SharingMode::eExclusive
           : vk::SharingMode::eConcurrent),
      ((indices.graphicsFamily == indices.presentFamily) ? 0u : 2u),
      ((indices.graphicsFamily == indices.presentFamily) ? nullptr
                                                         : queueFamilyIndices),
      swapChainSupport.capabilities.currentTransform,
      vk::CompositeAlphaFlagBitsKHR::eOpaque,
      presentMode,
      true};

  if (device.createSwapchainKHR(&createInfo, nullptr, &swapChain) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to create swap chain!");
  }

  swapChainImages = device.getSwapchainImagesKHR(swapChain);
  swapChainImageFormat = surfaceFormat.format;
  swapChainExtent = extent;
}

void ApplicationWindow::createImageViews() {
  swapChainImageViews.resize(swapChainImages.size());

  for (size_t i = 0; i < swapChainImages.size(); i++) {
    swapChainImageViews[i] =
        createImageView(swapChainImages[i], swapChainImageFormat,
                        vk::ImageAspectFlagBits::eColor, 1u);
  }
}

void ApplicationWindow::createRenderPass() {
  auto colorAttachment =
      vk::AttachmentDescription{vk::AttachmentDescriptionFlags{},
                                swapChainImageFormat,
                                msaaSamples,
                                vk::AttachmentLoadOp::eClear,
                                vk::AttachmentStoreOp::eStore,
                                vk::AttachmentLoadOp::eDontCare,
                                vk::AttachmentStoreOp::eDontCare,
                                vk::ImageLayout::eUndefined,
                                vk::ImageLayout::eColorAttachmentOptimal};

  auto colorAttachmentRef =
      vk::AttachmentReference{0u, vk::ImageLayout::eColorAttachmentOptimal};

  auto depthAttachment = vk::AttachmentDescription{
      vk::AttachmentDescriptionFlags{},
      findDepthFormat(),
      msaaSamples,
      vk::AttachmentLoadOp::eClear,
      vk::AttachmentStoreOp::eDontCare,
      vk::AttachmentLoadOp::eDontCare,
      vk::AttachmentStoreOp::eDontCare,
      vk::ImageLayout::eUndefined,
      vk::ImageLayout::eDepthStencilAttachmentOptimal};

  auto depthAttachmentRef = vk::AttachmentReference{
      1u, vk::ImageLayout::eDepthStencilAttachmentOptimal};

  auto colorAttachmentResolve = vk::AttachmentDescription{
      vk::AttachmentDescriptionFlags{}, swapChainImageFormat,
      vk::SampleCountFlagBits::e1,      vk::AttachmentLoadOp::eDontCare,
      vk::AttachmentStoreOp::eStore,    vk::AttachmentLoadOp::eDontCare,
      vk::AttachmentStoreOp::eDontCare, vk::ImageLayout::eUndefined,
      vk::ImageLayout::ePresentSrcKHR};

  auto colorAttachmentResolveRef =
      vk::AttachmentReference{2u, vk::ImageLayout::eColorAttachmentOptimal};

  std::array<vk::AttachmentDescription, 3u> attachments{
      colorAttachment, depthAttachment, colorAttachmentResolve};

  auto subpass = vk::SubpassDescription{vk::SubpassDescriptionFlags{},
                                        vk::PipelineBindPoint::eGraphics,
                                        0u,
                                        nullptr,
                                        1u,
                                        &colorAttachmentRef,
                                        &colorAttachmentResolveRef,
                                        &depthAttachmentRef};

  auto dependency = vk::SubpassDependency{
      vk::SubpassExternal,
      0u,
      vk::PipelineStageFlags{vk::PipelineStageFlagBits::eColorAttachmentOutput |
                             vk::PipelineStageFlagBits::eLateFragmentTests},
      vk::PipelineStageFlags{vk::PipelineStageFlagBits::eColorAttachmentOutput |
                             vk::PipelineStageFlagBits::eEarlyFragmentTests},
      vk::AccessFlags{},
      vk::AccessFlags{vk::AccessFlagBits::eColorAttachmentRead |
                      vk::AccessFlagBits::eColorAttachmentWrite |
                      vk::AccessFlagBits::eDepthStencilAttachmentWrite},
      vk::DependencyFlags{}};

  auto renderPassInfo = vk::RenderPassCreateInfo{vk::RenderPassCreateFlags{},
                                                 attachments.size(),
                                                 attachments.data(),
                                                 1u,
                                                 &subpass,
                                                 1u,
                                                 &dependency};

  if (device.createRenderPass(&renderPassInfo, nullptr, &renderPass) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to create render pass!");
  }
}

void ApplicationWindow::createDescriptorSetLayout() {
  auto uboLayoutBinding = vk::DescriptorSetLayoutBinding{
      0u, vk::DescriptorType::eUniformBuffer, 1u,
      vk::ShaderStageFlags{vk::ShaderStageFlagBits::eVertex}, nullptr};

  auto samplerLayoutBinding = vk::DescriptorSetLayoutBinding{
      1u, vk::DescriptorType::eCombinedImageSampler, 1u,
      vk::ShaderStageFlags{vk::ShaderStageFlagBits::eFragment}, nullptr};

  std::array<vk::DescriptorSetLayoutBinding, 2u> bindings = {
      uboLayoutBinding, samplerLayoutBinding};

  auto layoutInfo = vk::DescriptorSetLayoutCreateInfo{
      vk::DescriptorSetLayoutCreateFlags{}, bindings.size(), bindings.data()};

  if (device.createDescriptorSetLayout(
          &layoutInfo, nullptr, &descriptorSetLayout) != vk::Result::eSuccess) {
    throw std::runtime_error("failed to create descriptor set layout!");
  }
}

void ApplicationWindow::createGraphicsPipeline() {
  auto vertShaderModule = createShaderModule(std::span{shader_vert});
  auto fragShaderModule = createShaderModule(std::span{shader_frag});

  auto vertShaderStageInfo = vk::PipelineShaderStageCreateInfo{
      vk::PipelineShaderStageCreateFlags{}, vk::ShaderStageFlagBits::eVertex,
      vertShaderModule, "main"};

  auto fragShaderStageInfo = vk::PipelineShaderStageCreateInfo{
      vk::PipelineShaderStageCreateFlags{}, vk::ShaderStageFlagBits::eFragment,
      fragShaderModule, "main"};

  std::array<vk::PipelineShaderStageCreateInfo, 2> shaderStages = {
      {vertShaderStageInfo, fragShaderStageInfo}};

  auto bindingDescription = Vertex::getBindingDescription();
  auto attributeDescriptions = Vertex::getAttributeDescriptions();

  auto vertexInputInfo = vk::PipelineVertexInputStateCreateInfo{
      vk::PipelineVertexInputStateCreateFlags{}, 1u, &bindingDescription,
      static_cast<uint32_t>(attributeDescriptions.size()),
      attributeDescriptions.data()};

  auto inputAssembly = vk::PipelineInputAssemblyStateCreateInfo{
      vk::PipelineInputAssemblyStateCreateFlags{},
      vk::PrimitiveTopology::eTriangleList, false};

  auto viewport = vk::Viewport{
      0.0f, 0.0f, (float)swapChainExtent.width, (float)swapChainExtent.height,
      0.0f, 1.0f};

  auto scissor = vk::Rect2D{{0, 0}, swapChainExtent};

  auto viewportState = vk::PipelineViewportStateCreateInfo{
      vk::PipelineViewportStateCreateFlags{}, 1u, &viewport, 1u, &scissor};

  auto rasterizer = vk::PipelineRasterizationStateCreateInfo{
      vk::PipelineRasterizationStateCreateFlags{},
      vk::False,
      vk::False,
      vk::PolygonMode::eFill,
      vk::CullModeFlagBits::eBack,
      vk::FrontFace::eCounterClockwise,
      vk::False,
      0.0f,
      0.0f,
      0.0f,
      1.0f};

  auto multisampling = vk::PipelineMultisampleStateCreateInfo{
      vk::PipelineMultisampleStateCreateFlags{}, msaaSamples, vk::True, 0.2f};

  auto depthStencil =
      vk::PipelineDepthStencilStateCreateInfo{{},
                                              vk::True,
                                              vk::True,
                                              vk::CompareOp::eLess,
                                              vk::False,
                                              false,
                                              vk::StencilOp::eKeep,
                                              vk::StencilOp::eKeep,
                                              0.0f,
                                              1.0f,
                                              nullptr};

  auto colorBlendAttachment = vk::PipelineColorBlendAttachmentState{false};
  colorBlendAttachment.setColorWriteMask(
      vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
      vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);

  auto colorBlending = vk::PipelineColorBlendStateCreateInfo{
      vk::PipelineColorBlendStateCreateFlags{},
      vk::False,
      vk::LogicOp::eCopy,
      1u,
      &colorBlendAttachment,
      {0.0f, 0.0f, 0.0f, 0.0f}};

  std::array<vk::DynamicState, 2> dynamicStates = {vk::DynamicState::eViewport,
                                                   vk::DynamicState::eScissor};
  auto dynamicState = vk::PipelineDynamicStateCreateInfo{
      {}, dynamicStates.size(), dynamicStates.data()};

  auto pipelineLayoutInfo = vk::PipelineLayoutCreateInfo{
      vk::PipelineLayoutCreateFlags{}, 1u, &descriptorSetLayout};

  if (device.createPipelineLayout(&pipelineLayoutInfo, nullptr,
                                  &pipelineLayout) != vk::Result::eSuccess) {
    throw std::runtime_error("failed to create pipeline layout!");
  }

  auto pipelineInfo = vk::GraphicsPipelineCreateInfo{vk::PipelineCreateFlags{},
                                                     shaderStages.size(),
                                                     shaderStages.data(),
                                                     &vertexInputInfo,
                                                     &inputAssembly,
                                                     nullptr,
                                                     &viewportState,
                                                     &rasterizer,
                                                     &multisampling,
                                                     &depthStencil,
                                                     &colorBlending,
                                                     &dynamicState,
                                                     pipelineLayout,
                                                     renderPass,
                                                     0u};

  if (device.createGraphicsPipelines({}, 1, &pipelineInfo, nullptr,
                                     &graphicsPipeline) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to create graphics pipeline!");
  }

  device.destroy(fragShaderModule);
  device.destroy(vertShaderModule);
}

void ApplicationWindow::createFramebuffers() {
  swapChainFramebuffers.resize(swapChainImageViews.size());

  for (size_t i = 0; i < swapChainImageViews.size(); i++) {
    std::array<vk::ImageView, 3u> attachments = {colorImageView, depthImageView,
                                                 swapChainImageViews[i]};
    auto createInfo = vk::FramebufferCreateInfo{vk::FramebufferCreateFlags{},
                                                renderPass,
                                                attachments.size(),
                                                attachments.data(),
                                                swapChainExtent.width,
                                                swapChainExtent.height,
                                                1u};

    if (device.createFramebuffer(&createInfo, nullptr,
                                 &swapChainFramebuffers[i]) !=
        vk::Result::eSuccess) {
      throw std::runtime_error("failed to create framebuffer!");
    }
  }
}

void ApplicationWindow::createCommandPool() {
  QueueFamilyIndices queueFamilyIndices = findQueueFamilies(physicalDevice);

  auto createInfo = vk::CommandPoolCreateInfo{
      vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
      queueFamilyIndices.graphicsFamily.value()};

  if (device.createCommandPool(&createInfo, nullptr, &commandPool) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to create graphics command pool!");
  }
}

void ApplicationWindow::createDepthResources() {
  vk::Format depthFormat = findDepthFormat();

  createImage(swapChainExtent.width, swapChainExtent.height, msaaSamples, 1u,
              depthFormat, vk::ImageTiling::eOptimal,
              vk::ImageUsageFlagBits::eDepthStencilAttachment,
              vk::MemoryPropertyFlagBits::eDeviceLocal, depthImage,
              depthImageMemory);
  depthImageView = createImageView(depthImage, depthFormat,
                                   vk::ImageAspectFlagBits::eDepth, 1u);

  transitionImageLayout(depthImage, depthFormat, vk::ImageLayout::eUndefined,
                        vk::ImageLayout::eDepthStencilAttachmentOptimal, 1u);
}

void ApplicationWindow::createColorResources() {
  createImage(swapChainExtent.width, swapChainExtent.height, msaaSamples, 1,
              swapChainImageFormat, vk::ImageTiling::eOptimal,
              vk::ImageUsageFlagBits::eTransientAttachment |
                  vk::ImageUsageFlagBits::eColorAttachment,
              vk::MemoryPropertyFlagBits::eDeviceLocal, colorImage,
              colorImageMemory);
  colorImageView = createImageView(colorImage, swapChainImageFormat,
                                   vk::ImageAspectFlagBits::eColor, 1);
}

void ApplicationWindow::createTextureImage() {
  int texWidth, texHeight, texChannels;
  stbi_uc *pixels = stbi_load(TEXTURE_PATH.c_str(), &texWidth, &texHeight,
                              &texChannels, STBI_rgb_alpha);
  vk::DeviceSize imageSize = texWidth * texHeight * 4u;

  if (!pixels) {
    throw std::runtime_error("failed to load texture image");
  }

  vk::Buffer stagingBuffer;
  vk::DeviceMemory stagingBufferMemory;
  createBuffer(imageSize, vk::BufferUsageFlagBits::eTransferSrc,
               vk::MemoryPropertyFlagBits::eHostVisible |
                   vk::MemoryPropertyFlagBits::eHostCoherent,
               stagingBuffer, stagingBufferMemory);

  void *data;
  device.mapMemory(stagingBufferMemory, 0u, imageSize, vk::MemoryMapFlags{},
                   &data);
  memcpy(data, pixels, static_cast<size_t>(imageSize));
  device.unmapMemory(stagingBufferMemory);

  stbi_image_free(pixels);

  mipLevels = static_cast<uint32_t>(
                  std::floor(std::log2(std::max(texWidth, texHeight)))) +
              1u;

  createImage(texWidth, texHeight, vk::SampleCountFlagBits::e1, mipLevels,
              vk::Format::eR8G8B8A8Srgb, vk::ImageTiling::eOptimal,
              vk::ImageUsageFlagBits::eTransferSrc |
                  vk::ImageUsageFlagBits::eTransferDst |
                  vk::ImageUsageFlagBits::eSampled,
              vk::MemoryPropertyFlagBits::eDeviceLocal, textureImage,
              textureImageMemory);

  transitionImageLayout(textureImage, vk::Format::eR8G8B8A8Srgb,
                        vk::ImageLayout::eUndefined,
                        vk::ImageLayout::eTransferDstOptimal, mipLevels);
  copyBufferToImage(stagingBuffer, textureImage,
                    static_cast<uint32_t>(texWidth),
                    static_cast<uint32_t>(texHeight));
  // Transition to ReadOnlyOptimal while generating mipmaps

  device.destroy(stagingBuffer);
  device.free(stagingBufferMemory);

  generateMipMaps(textureImage, vk::Format::eR8G8B8A8Srgb, texWidth, texHeight,
                  mipLevels);
}

void ApplicationWindow::createTextureImageView() {
  textureImageView =
      createImageView(textureImage, vk::Format::eR8G8B8A8Srgb,
                      vk::ImageAspectFlagBits::eColor, mipLevels);
}

void ApplicationWindow::createTextureSampler() {
  auto deviceProps = physicalDevice.getProperties();

  auto samplerInfo =
      vk::SamplerCreateInfo{vk::SamplerCreateFlagBits{},
                            vk::Filter::eLinear,
                            vk::Filter::eLinear,
                            vk::SamplerMipmapMode::eLinear,
                            vk::SamplerAddressMode::eRepeat,
                            vk::SamplerAddressMode::eRepeat,
                            vk::SamplerAddressMode::eRepeat,
                            0.0f,
                            vk::True,
                            deviceProps.limits.maxSamplerAnisotropy,
                            vk::False,
                            vk::CompareOp::eAlways,
                            0.0f,
                            vk::LodClampNone,
                            vk::BorderColor::eIntOpaqueBlack,
                            vk::False};

  if (device.createSampler(&samplerInfo, nullptr, &textureSampler) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("Failed to create texture sampler");
  }
}

vk::ImageView
ApplicationWindow::createImageView(vk::Image image, vk::Format format,
                                   vk::ImageAspectFlags aspectFlags,
                                   uint32_t mipLevels) {
  auto viewInfo = vk::ImageViewCreateInfo{
      vk::ImageViewCreateFlagBits{},
      image,
      vk::ImageViewType::e2D,
      format,
      {},
      vk::ImageSubresourceRange{vk::ImageAspectFlagBits::eColor, 0u, mipLevels,
                                0u, 1u}};

  vk::ImageView imageView;
  if (device.createImageView(&viewInfo, nullptr, &imageView) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("Failed to create texture image view");
  }

  return imageView;
}

void ApplicationWindow::createImage(
    uint32_t width, uint32_t height, vk::SampleCountFlagBits numSamples,
    uint32_t mipLevels, vk::Format format, vk::ImageTiling tiling,
    vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties,
    vk::Image &image, vk::DeviceMemory &imageMemory) {
  auto imageInfo = vk::ImageCreateInfo{vk::ImageCreateFlags{},
                                       vk::ImageType::e2D,
                                       format,
                                       vk::Extent3D{width, height, 1u},
                                       mipLevels,
                                       1u,
                                       numSamples,
                                       tiling,
                                       usage,
                                       vk::SharingMode::eExclusive,
                                       0,
                                       nullptr,
                                       vk::ImageLayout::eUndefined,
                                       nullptr};

  if (device.createImage(&imageInfo, nullptr, &image) != vk::Result::eSuccess) {
    throw std::runtime_error("failed to create image");
  }

  auto memoryRequirements = device.getImageMemoryRequirements(image);

  auto allocInfo = vk::MemoryAllocateInfo{
      memoryRequirements.size,
      findMemoryType(memoryRequirements.memoryTypeBits, properties)};

  if (device.allocateMemory(&allocInfo, nullptr, &imageMemory) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to allocate image memory");
  }

  device.bindImageMemory(image, imageMemory, 0);
}

void ApplicationWindow::generateMipMaps(vk::Image image, vk::Format imageFormat,
                                        int32_t texWidth, int32_t texHeight,
                                        uint32_t mipLevels) {
  auto formatProperties = physicalDevice.getFormatProperties(imageFormat);

  if (!(formatProperties.optimalTilingFeatures &
        vk::FormatFeatureFlagBits::eSampledImageFilterLinear)) {
    throw std::runtime_error("Linear blitting not supported by texture format");
  }

  auto commandBuffer = beginSingleTimeCommands();

  auto barrier =
      vk::ImageMemoryBarrier{vk::AccessFlagBits::eTransferWrite,
                             vk::AccessFlagBits::eTransferRead,
                             vk::ImageLayout::eTransferDstOptimal,
                             vk::ImageLayout::eTransferSrcOptimal,
                             vk::QueueFamilyIgnored,
                             vk::QueueFamilyIgnored,
                             image,
                             {vk::ImageAspectFlagBits::eColor, 0u, 1u, 0u, 1u}};

  int32_t previousMipWidth = texWidth;
  int32_t previousMipHeight = texHeight;

  for (uint32_t i = 1u; i < mipLevels; ++i) {
    int32_t currentMipWidth = previousMipWidth;
    int32_t currentMipHeight = previousMipHeight;

    if (currentMipWidth > 1) {
      currentMipWidth /= 2;
    }
    if (currentMipHeight > 1) {
      currentMipHeight /= 2;
    }

    barrier.subresourceRange.baseMipLevel = i - 1;
    barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
    barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal;
    barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
    barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;

    commandBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer,
                                  vk::PipelineStageFlagBits::eTransfer, {}, {},
                                  {}, barrier);

    auto blit = vk::ImageBlit{
        vk::ImageSubresourceLayers{vk::ImageAspectFlagBits::eColor, i - 1, 0,
                                   1},
        {{{0, 0, 0}, {previousMipWidth, previousMipHeight, 1}}},
        vk::ImageSubresourceLayers{vk::ImageAspectFlagBits::eColor, i, 0, 1},
        {{{0, 0, 0}, {currentMipWidth, currentMipHeight, 1}}}};

    commandBuffer.blitImage(image, vk::ImageLayout::eTransferSrcOptimal, image,
                            vk::ImageLayout::eTransferDstOptimal, blit,
                            vk::Filter::eLinear);

    barrier.subresourceRange.baseMipLevel = i - 1;
    barrier.oldLayout = vk::ImageLayout::eTransferSrcOptimal;
    barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    barrier.srcAccessMask = vk::AccessFlagBits::eTransferRead;
    barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

    commandBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer,
                                  vk::PipelineStageFlagBits::eFragmentShader,
                                  {}, {}, {}, barrier);

    previousMipWidth = currentMipWidth;
    previousMipHeight = currentMipHeight;
  }

  barrier.subresourceRange.baseMipLevel = mipLevels - 1u;
  barrier.oldLayout = vk::ImageLayout::eTransferDstOptimal;
  barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
  barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
  barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;

  commandBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer,
                                vk::PipelineStageFlagBits::eFragmentShader, {},
                                {}, {}, barrier);

  endSingleTimeCommands(commandBuffer);
}

void ApplicationWindow::loadModel() {
  tinyobj::attrib_t attrib;
  std::vector<tinyobj::shape_t> shapes;
  std::vector<tinyobj::material_t> materials;
  std::string warn, err;

  if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err,
                        MODEL_PATH.c_str())) {
    throw std::runtime_error(warn + err);
  }

  std::unordered_map<Vertex, uint32_t> uniqueVertices{};

  for (const auto &shape : shapes) {
    for (const auto &index : shape.mesh.indices) {
      Vertex vertex{{attrib.vertices[(3 * index.vertex_index) + 0],
                     attrib.vertices[(3 * index.vertex_index) + 1],
                     attrib.vertices[(3 * index.vertex_index) + 2]},
                    {1.0f, 1.0f, 1.0f},
                    {attrib.texcoords[(2 * index.texcoord_index) + 0],
                     1.0f - attrib.texcoords[(2 * index.texcoord_index) + 1]}};

      if (uniqueVertices.count(vertex) == 0) {
        uniqueVertices[vertex] = vertices.size();
        vertices.push_back(vertex);
      }
      indices.push_back(uniqueVertices[vertex]);
    }
  }
}

void ApplicationWindow::createVertexBuffer() {
  vk::DeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

  vk::Buffer stagingBuffer;
  vk::DeviceMemory stagingBufferMemory;
  createBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferSrc,
               vk::MemoryPropertyFlagBits::eHostVisible |
                   vk::MemoryPropertyFlagBits::eHostCoherent,
               stagingBuffer, stagingBufferMemory);

  void *data;
  vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
  memcpy(data, vertices.data(), (size_t)bufferSize);
  vkUnmapMemory(device, stagingBufferMemory);

  createBuffer(bufferSize,
               vk::BufferUsageFlagBits::eTransferDst |
                   vk::BufferUsageFlagBits::eVertexBuffer,
               vk::MemoryPropertyFlagBits::eDeviceLocal, vertexBuffer,
               vertexBufferMemory);

  copyBuffer(stagingBuffer, vertexBuffer, bufferSize);

  vkDestroyBuffer(device, stagingBuffer, nullptr);
  vkFreeMemory(device, stagingBufferMemory, nullptr);
}

void ApplicationWindow::createIndexBuffer() {
  vk::DeviceSize bufferSize = sizeof(indices[0]) * indices.size();

  vk::Buffer stagingBuffer;
  vk::DeviceMemory stagingBufferMemory;
  createBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferSrc,
               vk::MemoryPropertyFlagBits::eHostVisible |
                   vk::MemoryPropertyFlagBits::eHostCoherent,
               stagingBuffer, stagingBufferMemory);

  void *data;
  vkMapMemory(device, stagingBufferMemory, 0, bufferSize, 0, &data);
  memcpy(data, indices.data(), (size_t)bufferSize);
  vkUnmapMemory(device, stagingBufferMemory);

  createBuffer(bufferSize,
               vk::BufferUsageFlagBits::eTransferDst |
                   vk::BufferUsageFlagBits::eIndexBuffer,
               vk::MemoryPropertyFlagBits::eDeviceLocal, indexBuffer,
               indexBufferMemory);

  copyBuffer(stagingBuffer, indexBuffer, bufferSize);

  vkDestroyBuffer(device, stagingBuffer, nullptr);
  vkFreeMemory(device, stagingBufferMemory, nullptr);
}

void ApplicationWindow::createUniformBuffers() {
  vk::DeviceSize bufferSize = sizeof(UniformBufferObject);

  uniformBuffers.resize(swapChainImages.size());
  uniformBuffersMemory.resize(swapChainImages.size());

  for (size_t i = 0; i < swapChainImages.size(); i++) {
    createBuffer(bufferSize, vk::BufferUsageFlagBits::eUniformBuffer,
                 vk::MemoryPropertyFlagBits::eHostVisible |
                     vk::MemoryPropertyFlagBits::eHostCoherent,
                 uniformBuffers[i], uniformBuffersMemory[i]);
  }
}

void ApplicationWindow::createDescriptorPool() {
  std::array<vk::DescriptorPoolSize, 2u> poolSizes = {
      vk::DescriptorPoolSize{vk::DescriptorType::eUniformBuffer,
                             static_cast<uint32_t>(swapChainImages.size())},
      vk::DescriptorPoolSize{vk::DescriptorType::eCombinedImageSampler,
                             static_cast<uint32_t>(swapChainImages.size())}};

  auto poolInfo = vk::DescriptorPoolCreateInfo{
      {},
      static_cast<uint32_t>(swapChainImages.size()),
      poolSizes.size(),
      poolSizes.data()};

  if (device.createDescriptorPool(&poolInfo, nullptr, &descriptorPool) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to create descriptor pool!");
  }
}

void ApplicationWindow::createDescriptorSets() {
  std::vector<vk::DescriptorSetLayout> layouts(swapChainImages.size(),
                                               descriptorSetLayout);
  auto allocInfo = vk::DescriptorSetAllocateInfo{
      descriptorPool, static_cast<uint32_t>(swapChainImages.size()),
      layouts.data()};
  descriptorSets = device.allocateDescriptorSets(allocInfo);

  for (size_t i = 0; i < swapChainImages.size(); i++) {
    auto bufferInfo = vk::DescriptorBufferInfo{uniformBuffers[i], 0u,
                                               sizeof(UniformBufferObject)};
    auto imageInfo = vk::DescriptorImageInfo{textureSampler, textureImageView};

    std::array<vk::WriteDescriptorSet, 2u> descriptorWrites = {
        vk::WriteDescriptorSet{descriptorSets[i], 0u, 0u, 1u,
                               vk::DescriptorType::eUniformBuffer, nullptr,
                               &bufferInfo},
        vk::WriteDescriptorSet{descriptorSets[i], 1u, 0u, 1u,
                               vk::DescriptorType::eCombinedImageSampler,
                               &imageInfo, nullptr}};
    device.updateDescriptorSets(descriptorWrites.size(),
                                descriptorWrites.data(), 0u, nullptr);
  }
}

void ApplicationWindow::createBuffer(vk::DeviceSize size,
                                     vk::BufferUsageFlags usage,
                                     vk::MemoryPropertyFlags properties,
                                     vk::Buffer &buffer,
                                     vk::DeviceMemory &bufferMemory) {
  auto createInfo = vk::BufferCreateInfo{vk::BufferCreateFlags{}, size, usage,
                                         vk::SharingMode::eExclusive};

  if (device.createBuffer(&createInfo, nullptr, &buffer) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to create buffer!");
  }

  vk::MemoryRequirements memRequirements;
  device.getBufferMemoryRequirements(buffer, &memRequirements);

  auto allocInfo = vk::MemoryAllocateInfo{
      memRequirements.size,
      findMemoryType(memRequirements.memoryTypeBits, properties)};

  if (device.allocateMemory(&allocInfo, nullptr, &bufferMemory) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to allocate buffer memory!");
  }

  device.bindBufferMemory(buffer, bufferMemory, 0);
}

void ApplicationWindow::copyBuffer(vk::Buffer srcBuffer, vk::Buffer dstBuffer,
                                   vk::DeviceSize size) {
  auto commandBuffer = beginSingleTimeCommands();

  auto copyRegion = vk::BufferCopy{0u, 0u, size};
  commandBuffer.copyBuffer(srcBuffer, dstBuffer, copyRegion);

  endSingleTimeCommands(commandBuffer);
}

uint32_t ApplicationWindow::findMemoryType(uint32_t typeFilter,
                                           vk::MemoryPropertyFlags properties) {
  vk::PhysicalDeviceMemoryProperties memProperties;
  physicalDevice.getMemoryProperties(&memProperties);

  for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
    if ((typeFilter & (1u << i)) &&
        (memProperties.memoryTypes[i].propertyFlags & properties) ==
            properties) {
      return i;
    }
  }

  throw std::runtime_error("failed to find suitable memory type!");
}

vk::CommandBuffer ApplicationWindow::beginSingleTimeCommands() {
  auto allocInfo = vk::CommandBufferAllocateInfo{
      commandPool, vk::CommandBufferLevel::ePrimary, 1};

  vk::CommandBuffer commandBuffer;
  if (device.allocateCommandBuffers(&allocInfo, &commandBuffer) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to allocate command buffer!");
  }

  commandBuffer.begin(vk::CommandBufferBeginInfo{
      vk::CommandBufferUsageFlagBits::eOneTimeSubmit});

  return commandBuffer;
}

void ApplicationWindow::endSingleTimeCommands(vk::CommandBuffer commandBuffer) {
  commandBuffer.end();

  auto submitInfo =
      vk::SubmitInfo{0u, nullptr, nullptr, 1u, &commandBuffer, 0u, nullptr};

  if (graphicsQueue.submit(1u, &submitInfo, {}) != vk::Result::eSuccess) {
    throw std::runtime_error("failed to submit single-time command buffer!");
  }

  graphicsQueue.waitIdle();

  device.freeCommandBuffers(commandPool, commandBuffer);
}

void ApplicationWindow::transitionImageLayout(vk::Image image,
                                              vk::Format format,
                                              vk::ImageLayout oldLayout,
                                              vk::ImageLayout newLayout,
                                              uint32_t mipLevels) {
  auto commandBuffer = beginSingleTimeCommands();
  vk::PipelineStageFlags sourceStage, destinationStage;

  auto barrier = vk::ImageMemoryBarrier{
      {},
      {},
      oldLayout,
      newLayout,
      vk::QueueFamilyIgnored,
      vk::QueueFamilyIgnored,
      image,
      vk::ImageSubresourceRange{vk::ImageAspectFlagBits::eColor, 0u, mipLevels,
                                0u, 1u}};

  if (oldLayout == vk::ImageLayout::eUndefined &&
      newLayout == vk::ImageLayout::eTransferDstOptimal) {
    barrier.srcAccessMask = {};
    barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;
    sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
    destinationStage = vk::PipelineStageFlagBits::eTransfer;
  } else if (oldLayout == vk::ImageLayout::eTransferDstOptimal &&
             newLayout == vk::ImageLayout::eShaderReadOnlyOptimal) {
    barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
    barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
    sourceStage = vk::PipelineStageFlagBits::eTransfer;
    destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
  } else if (oldLayout == vk::ImageLayout::eUndefined &&
             newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal) {
    barrier.srcAccessMask = {};
    barrier.dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentRead |
                            vk::AccessFlagBits::eDepthStencilAttachmentWrite;
    sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
    destinationStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;
  } else {
    throw std::invalid_argument("unsupported layout transition");
  }

  commandBuffer.pipelineBarrier(sourceStage, destinationStage,
                                vk::DependencyFlags{}, nullptr, nullptr,
                                barrier);

  endSingleTimeCommands(commandBuffer);
}

void ApplicationWindow::copyBufferToImage(vk::Buffer buffer, vk::Image image,
                                          uint32_t width, uint32_t height) {
  auto commandBuffer = beginSingleTimeCommands();

  auto region =
      vk::BufferImageCopy{0u, 0u,
                          0u, {vk::ImageAspectFlagBits::eColor, 0u, 0u, 1u},
                          0u, {width, height, 1u}};

  commandBuffer.copyBufferToImage(buffer, image,
                                  vk::ImageLayout::eTransferDstOptimal, region);

  endSingleTimeCommands(commandBuffer);
}

void ApplicationWindow::createCommandBuffers() {
  commandBuffers.resize(swapChainFramebuffers.size());

  auto allocInfo = vk::CommandBufferAllocateInfo{
      commandPool, vk::CommandBufferLevel::ePrimary,
      static_cast<uint32_t>(commandBuffers.size())};

  if (device.allocateCommandBuffers(&allocInfo, commandBuffers.data()) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to allocate command buffers!");
  }
}

void ApplicationWindow::recordCommandBuffer(vk::CommandBuffer commandBuffer,
                                            uint32_t imageIndex) {
  auto beginInfo = vk::CommandBufferBeginInfo{
      vk::CommandBufferUsageFlagBits::eSimultaneousUse};

  if (commandBuffer.begin(&beginInfo) != vk::Result::eSuccess) {
    throw std::runtime_error("failed to begin recording command buffer!");
  }

  std::array<vk::ClearValue, 2u> clearValues{
      vk::ClearColorValue{std::array<float, 4>{0.0f, 0.0f, 0.0f, 1.0f}},
      vk::ClearColorValue{std::array<float, 4>{1.0f, 0.0f}}};

  auto renderPassInfo =
      vk::RenderPassBeginInfo{renderPass, swapChainFramebuffers[imageIndex],
                              vk::Rect2D{{0, 0}, swapChainExtent},
                              clearValues.size(), clearValues.data()};

  commandBuffer.beginRenderPass(&renderPassInfo, vk::SubpassContents::eInline);

  commandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics,
                             graphicsPipeline);

  auto viewport = vk::Viewport{0.0f,
                               0.0f,
                               static_cast<float>(swapChainExtent.width),
                               static_cast<float>(swapChainExtent.height),
                               0.0f,
                               1.0f};
  commandBuffer.setViewport(0, viewport);

  auto scissor = vk::Rect2D{{0, 0}, swapChainExtent};
  commandBuffer.setScissor(0, scissor);

  vk::Buffer vertexBuffers[] = {vertexBuffer};
  vk::DeviceSize offsets[] = {0};
  commandBuffer.bindVertexBuffers(0u, 1u, vertexBuffers, offsets);

  commandBuffer.bindIndexBuffer(indexBuffer, 0u, vk::IndexType::eUint32);

  commandBuffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics,
                                   pipelineLayout, 0u, 1u,
                                   &descriptorSets[imageIndex], 0u, nullptr);

  commandBuffer.drawIndexed(static_cast<uint32_t>(indices.size()), 1u, 0u, 0u,
                            0u);

  commandBuffer.endRenderPass();

#ifdef VULKAN_HPP_NO_EXCEPTIONS
  if (commandBuffer.end() != vk::Result::eSuccess) {
    throw std::runtime_error("failed to record command buffer!");
  }
#else
  commandBuffer.end();
#endif
}

void ApplicationWindow::createSyncObjects() {
  imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
  renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
  inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

  auto semaphoreInfo = vk::SemaphoreCreateInfo{vk::SemaphoreCreateFlags{}};

  auto fenceInfo = vk::FenceCreateInfo{vk::FenceCreateFlagBits::eSignaled};

  for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++) {
    if ((device.createSemaphore(&semaphoreInfo, nullptr,
                                &imageAvailableSemaphores[i]) !=
         vk::Result::eSuccess) ||
        (device.createSemaphore(&semaphoreInfo, nullptr,
                                &renderFinishedSemaphores[i]) !=
         vk::Result::eSuccess) ||
        (device.createFence(&fenceInfo, nullptr, &inFlightFences[i]) !=
         vk::Result::eSuccess)) {
      throw std::runtime_error(
          "failed to create synchronization objects for a frame!");
    }
  }
}

void ApplicationWindow::updateUniformBuffer(uint32_t currentImage) {
  static auto startTime = std::chrono::high_resolution_clock::now();

  auto currentTime = std::chrono::high_resolution_clock::now();
  float time = std::chrono::duration<float, std::chrono::seconds::period>(
                   currentTime - startTime)
                   .count();

  glm::mat4 model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f),
                                glm::vec3(0.0f, 0.0f, 1.0f));
  glm::mat4 view =
      glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f),
                  glm::vec3(0.0f, 0.0f, 1.0f));
  glm::mat4 proj =
      glm::perspective(glm::radians(45.0f),
                       static_cast<float>(swapChainExtent.width) /
                           static_cast<float>(swapChainExtent.height),
                       0.1f, 10.0f);
  proj[1][1] *= -1;

  UniformBufferObject ubo = {model, view, proj};

  void *data;
  vkMapMemory(device, uniformBuffersMemory[currentImage], 0, sizeof(ubo), 0,
              &data);
  memcpy(data, &ubo, sizeof(ubo));
  vkUnmapMemory(device, uniformBuffersMemory[currentImage]);
}

void ApplicationWindow::drawFrame() {
  device.waitForFences(1u, &inFlightFences[currentFrame], vk::True,
                       std::numeric_limits<uint64_t>::max());

  uint32_t imageIndex;
  vk::Result result = device.acquireNextImageKHR(
      swapChain, std::numeric_limits<uint64_t>::max(),
      imageAvailableSemaphores[currentFrame], nullptr, &imageIndex);

  if (result == vk::Result::eErrorOutOfDateKHR) {
    recreateSwapChain();
    return;
  } else if (result != vk::Result::eSuccess &&
             result != vk::Result::eSuboptimalKHR) {
    throw std::runtime_error("failed to acquire swap chain image!");
  }

  updateUniformBuffer(imageIndex);

  device.resetFences(1u, &inFlightFences[currentFrame]);

  commandBuffers[currentFrame].reset({});
  recordCommandBuffer(commandBuffers[currentFrame], imageIndex);

  vk::Semaphore waitSemaphores[] = {imageAvailableSemaphores[currentFrame]};
  vk::PipelineStageFlags waitStages[] = {
      vk::PipelineStageFlagBits::eColorAttachmentOutput};
  vk::Semaphore signalSemaphores[] = {renderFinishedSemaphores[currentFrame]};

  auto submitInfo = vk::SubmitInfo{
      1u, waitSemaphores,  waitStages, 1u, &commandBuffers[currentFrame],
      1u, signalSemaphores};

  if (graphicsQueue.submit(1u, &submitInfo, inFlightFences[currentFrame]) !=
      vk::Result::eSuccess) {
    throw std::runtime_error("failed to submit draw command buffer!");
  }

  vk::SwapchainKHR swapChains[] = {swapChain};

  auto presentInfo =
      vk::PresentInfoKHR{1u, signalSemaphores, 1u, swapChains, &imageIndex};

  result = presentQueue.presentKHR(presentInfo);

  if (result == vk::Result::eErrorOutOfDateKHR ||
      result == vk::Result::eSuboptimalKHR || framebufferResized) {
    framebufferResized = false;
    recreateSwapChain();
  } else if (result != vk::Result::eSuccess) {
    throw std::runtime_error("failed to present swap chain image!");
  }

  currentFrame = (currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;

  sol::function f = lua["frame_end"];
  f();
}

vk::ShaderModule
ApplicationWindow::createShaderModule(std::span<const unsigned char> code) {
  auto createInfo = vk::ShaderModuleCreateInfo(
      vk::ShaderModuleCreateFlags{}, code.size(),
      reinterpret_cast<const uint32_t *>(code.data()));
  auto shaderModule = device.createShaderModule(createInfo);
  return shaderModule;
}

vk::SurfaceFormatKHR ApplicationWindow::chooseSwapSurfaceFormat(
    const std::vector<vk::SurfaceFormatKHR> &availableFormats) {
  for (const auto &availableFormat : availableFormats) {
    if (availableFormat.format == vk::Format::eB8G8R8A8Srgb &&
        availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear) {
      return availableFormat;
    }
  }

  return availableFormats[0];
}

vk::PresentModeKHR ApplicationWindow::chooseSwapPresentMode(
    const std::vector<vk::PresentModeKHR> &availablePresentModes) {
  vk::PresentModeKHR bestMode = vk::PresentModeKHR::eFifo;

  for (const auto &availablePresentMode : availablePresentModes) {
    if (availablePresentMode == vk::PresentModeKHR::eMailbox) {
      return availablePresentMode;
    } else if (availablePresentMode == vk::PresentModeKHR::eImmediate) {
      bestMode = availablePresentMode;
    }
  }

  return bestMode;
}

vk::Extent2D ApplicationWindow::chooseSwapExtent(
    const vk::SurfaceCapabilitiesKHR &capabilities) {
  if (capabilities.currentExtent.width !=
      std::numeric_limits<uint32_t>::max()) {
    return capabilities.currentExtent;
  } else {
    auto [width, height] = size();

    auto actualExtent =
        vk::Extent2D{std::max(capabilities.minImageExtent.width,
                              std::min(capabilities.maxImageExtent.width,
                                       static_cast<uint32_t>(width))),
                     std::max(capabilities.minImageExtent.height,
                              std::min(capabilities.maxImageExtent.height,
                                       static_cast<uint32_t>(height)))};

    return actualExtent;
  }
}

SwapChainSupportDetails
ApplicationWindow::querySwapChainSupport(vk::PhysicalDevice device) {
  SwapChainSupportDetails details;

  details.capabilities = device.getSurfaceCapabilitiesKHR(surface);
  details.formats = device.getSurfaceFormatsKHR(surface);
  details.presentModes = device.getSurfacePresentModesKHR(surface);

  return details;
}

bool ApplicationWindow::isDeviceSuitable(vk::PhysicalDevice device) {
  QueueFamilyIndices indices = findQueueFamilies(device);

  bool extensionsSupported = checkDeviceExtensionSupport(device);

  bool swapChainAdequate = false;
  if (extensionsSupported) {
    SwapChainSupportDetails swapChainSupport = querySwapChainSupport(device);
    swapChainAdequate = !swapChainSupport.formats.empty() &&
                        !swapChainSupport.presentModes.empty();
  }

  vk::PhysicalDeviceFeatures deviceFeatures = device.getFeatures();

  return indices.isComplete() && extensionsSupported && swapChainAdequate &&
         deviceFeatures.samplerAnisotropy;
}

bool ApplicationWindow::checkDeviceExtensionSupport(vk::PhysicalDevice device) {
  std::vector<vk::ExtensionProperties> availableExtensions =
      device.enumerateDeviceExtensionProperties();

  std::set<std::string> requiredExtensions(deviceExtensions.begin(),
                                           deviceExtensions.end());

  for (const auto &extension : availableExtensions) {
    requiredExtensions.erase(extension.extensionName);
  }

  return requiredExtensions.empty();
}

QueueFamilyIndices
ApplicationWindow::findQueueFamilies(vk::PhysicalDevice device) {
  QueueFamilyIndices indices;

  auto queueFamilies = device.getQueueFamilyProperties();

  int i = 0;
  for (const auto &queueFamily : queueFamilies) {
    if (queueFamily.queueCount > 0u &&
        queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) {
      indices.graphicsFamily = i;
    }

    vk::Bool32 presentSupport = device.getSurfaceSupportKHR(i, surface);
    if (queueFamily.queueCount > 0 && presentSupport == VK_TRUE) {
      indices.presentFamily = i;
    }

    if (indices.isComplete()) {
      break;
    }

    i++;
  }

  return indices;
}

std::vector<const char *> ApplicationWindow::getRequiredExtensions() {
  uint32_t glfwExtensionCount = 0;
  const char **glfwExtensions;
  glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

  std::vector<const char *> extensions(glfwExtensions,
                                       glfwExtensions + glfwExtensionCount);

  if (enableValidationLayers) {
    extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
  }

  return extensions;
}

bool ApplicationWindow::checkValidationLayerSupport() {
  std::vector<vk::LayerProperties> availableLayers =
      vk::enumerateInstanceLayerProperties();

  for (const char *layerName : validationLayers) {
    bool layerFound = false;

    for (const auto &layerProperties : availableLayers) {
      if (strcmp(layerName, layerProperties.layerName) == 0) {
        layerFound = true;
        break;
      }
    }

    if (!layerFound) {
      return false;
    }
  }

  return true;
}

vk::Format ApplicationWindow::findSupportedFormat(
    const std::vector<vk::Format> &candidates, vk::ImageTiling tiling,
    vk::FormatFeatureFlags features) {
  for (vk::Format format : candidates) {
    vk::FormatProperties props;
    physicalDevice.getFormatProperties(format, &props);

    if (tiling == vk::ImageTiling::eLinear &&
        (props.linearTilingFeatures & features) == features) {
      return format;
    } else if (tiling == vk::ImageTiling::eOptimal &&
               (props.optimalTilingFeatures & features) == features) {
      return format;
    }
  }

  throw std::runtime_error("Failed to find supported format");
}

vk::Format ApplicationWindow::findDepthFormat() {
  return findSupportedFormat(
      {vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint,
       vk::Format::eD24UnormS8Uint},
      vk::ImageTiling::eOptimal,
      vk::FormatFeatureFlagBits::eDepthStencilAttachment);
}

bool ApplicationWindow::hasStencilComponent(vk::Format format) {
  return format == vk::Format::eD32SfloatS8Uint ||
         format == vk::Format::eD24UnormS8Uint;
}

vk::SampleCountFlagBits ApplicationWindow::getMaxUsableSampleCount() {
  auto props = physicalDevice.getProperties();
  auto counts = props.limits.framebufferColorSampleCounts &
                props.limits.framebufferDepthSampleCounts;

  const std::array<vk::SampleCountFlagBits, 6> possibleCounts = {
      {vk::SampleCountFlagBits::e64, vk::SampleCountFlagBits::e32,
       vk::SampleCountFlagBits::e16, vk::SampleCountFlagBits::e8,
       vk::SampleCountFlagBits::e4, vk::SampleCountFlagBits::e2}};

  for (const auto c : possibleCounts) {
    if (counts & c) {
      return c;
    }
  }

  return vk::SampleCountFlagBits::e1;
}

VKAPI_ATTR VkBool32 VKAPI_CALL ApplicationWindow::debugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT *pCallbackData,
    void *pUserData) {
  std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;

  return VK_FALSE;
}
