//
// Created by rleigh on 02/10/2020.
//

#ifndef VLK_FILEUTILS_H
#define VLK_FILEUTILS_H

#include <fstream>
#include <string>
#include <vector>

std::vector<char> readFile(const std::string &filename);

#endif // VLK_FILEUTILS_H
