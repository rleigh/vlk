#include "ApplicationWindow.h"

#include <exception>
#include <iostream>

int main() {
  try {
    ApplicationWindow app("Vulkan test");
    app.run();
  } catch (const std::exception &e) {
    std::cerr << e.what() << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
