//
// Created by rleigh on 29/06/2020.
//

#ifndef VLK_WINDOW_H
#define VLK_WINDOW_H

#define VULKAN_HPP_DISPATCH_LOADER_DYNAMIC 1
#include <vulkan/vulkan.hpp>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <utility>

class Window {
public:
  static constexpr int WIDTH = 800;
  static constexpr int HEIGHT = 600;

  explicit Window(const char *title, int width = WIDTH, int height = HEIGHT);

  virtual ~Window();

  virtual void windowResize(int width, int height);

  virtual void windowFocus(bool focus);

  virtual void windowClose();

  virtual void keyPress(int key, int scancode, int action, int mods);

  virtual void mouseButtonPress(int button, int action, int mods);

  virtual void scroll(double xoffset, double yoffset);

  virtual void cursorPosition(double xpos, double ypos);

  virtual void cursorEnter(bool enter);

  [[nodiscard]] std::pair<int, int> size() const;

  virtual void run();

  vk::Result createSurface(vk::Instance instance, vk::SurfaceKHR &surface);

  virtual void draw();

  virtual void waitEvents();

private:
  GLFWwindow *window;

  static void windowResizeCallback(GLFWwindow *window, int width, int height);

  static void windowCloseCallback(GLFWwindow *window);

  static void windowFocusCallback(GLFWwindow *window, int focus);

  static void keyPressCallback(GLFWwindow *window, int key, int scancode,
                               int action, int mods);

  static void mouseButtonPressCallback(GLFWwindow *window, int button,
                                       int action, int mods);

  static void scrollCallback(GLFWwindow *window, double xoffset,
                             double yoffset);

  static void cursorPositionCallback(GLFWwindow *window, double xpos,
                                     double ypos);

  static void cursorEnterCallback(GLFWwindow *window, int enter);

  void mainLoop();
};

#endif // VLK_WINDOW_H
