//
// Created by rleigh on 06/07/2022.
//

#ifndef VLK_UNIFORMBUFFEROBJECT_H
#define VLK_UNIFORMBUFFEROBJECT_H

#include <glm/glm.hpp>

struct UniformBufferObject {
  alignas(16) glm::mat4 model;
  alignas(16) glm::mat4 view;
  alignas(16) glm::mat4 proj;
};

#endif // VLK_UNIFORMBUFFEROBJECT_H
