function(lua_compile input output)
    add_custom_command(
            OUTPUT ${output}
            DEPENDS ${input}
            COMMAND ${LUAC_EXECUTABLE} -o ${output} ${input})
endfunction()

set(lua_scripts
        main.lua
        )

set(lua_objects)
foreach(lua_script IN LISTS lua_scripts)
    get_filename_component(lua_object_name "${lua_script}" NAME_WLE)
    set(lua_object "${CMAKE_CURRENT_BINARY_DIR}/${lua_object_name}.luao")
    lua_compile(${CMAKE_CURRENT_SOURCE_DIR}/${lua_script} ${lua_object})
    list(APPEND lua_objects ${lua_object})
endforeach()

add_custom_target(lua-compile ALL DEPENDS ${lua_objects})


set(gensrc)
foreach(lua_object IN LISTS lua_objects)
    get_filename_component(lua_object_name "${lua_object}" NAME_WLE)
    string(REPLACE . _ lua_object_name lua_${lua_object_name})
    vlk_encode_string(INPUT ${lua_object} NAME ${lua_object_name}
            HEADER_OUTPUT gen_header SOURCE_OUTPUT gen_source BINARY
            )

    list(APPEND gensrc ${gen_header} ${gen_source})
endforeach()

add_library(lua_scripts STATIC)

target_sources(lua_scripts PRIVATE ${gensrc})

target_include_directories(lua_scripts PUBLIC ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
