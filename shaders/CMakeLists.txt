function(compile_shader name)
    set(in ${CMAKE_CURRENT_SOURCE_DIR}/${name})
    set(out "${CMAKE_CURRENT_BINARY_DIR}/${name}.spv")
    add_custom_command(OUTPUT "${out}"
            COMMAND glslangValidator "${in}" -V -o "${out}"
            DEPENDS "${in}")
    list(APPEND spirv_programs "${out}")
    set(spirv_programs "${spirv_programs}" PARENT_SCOPE)
endfunction()

function(compile_shaders)
    foreach (shader IN LISTS ARGN)
        compile_shader("${shader}")
    endforeach ()
    set(spirv_programs "${spirv_programs}" PARENT_SCOPE)
endfunction()

compile_shaders(shader.frag shader.vert)

add_custom_target(spirv-compile ALL DEPENDS ${spirv_programs})

set(gensrc)
foreach(spirv_program IN LISTS spirv_programs)
    get_filename_component(spirv_program_name "${spirv_program}" NAME_WLE)
    string(REPLACE . _ spirv_program_name ${spirv_program_name})
    vlk_encode_string(INPUT ${spirv_program} NAME ${spirv_program_name}
            HEADER_OUTPUT gen_header SOURCE_OUTPUT gen_source BINARY
            )

    list(APPEND gensrc ${gen_header} ${gen_source})
endforeach()

add_library(shaders STATIC)

target_sources(shaders PRIVATE ${gensrc})

target_include_directories(shaders PUBLIC ${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})
